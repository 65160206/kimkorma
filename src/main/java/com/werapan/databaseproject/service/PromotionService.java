/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.PromotionDao;
import com.werapan.databaseproject.dao.PromotionDao;
import com.werapan.databaseproject.model.Promotion;
import com.werapan.databaseproject.model.Promotion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class PromotionService {
    public Promotion getById(int id) {
        PromotionDao promotionDao = new  PromotionDao();
        return promotionDao.get(id);
    }

    public ArrayList<Promotion> getPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" PMT_ID asc");
    }
    public Promotion getByName(String promotionName) {
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion = promotionDao.getByName(promotionName);
        return promotion;
    }
  

    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}
