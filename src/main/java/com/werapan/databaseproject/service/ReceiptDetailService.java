/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ReceiptDao;
import com.werapan.databaseproject.dao.ReceiptDetailDao;
import com.werapan.databaseproject.model.Receipt;
import com.werapan.databaseproject.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author wittaya
 */
public class ReceiptDetailService {
    public ReceiptDetail getById(int id) {
        ReceiptDetailDao receiptDetailDao = new  ReceiptDetailDao();
        return receiptDetailDao.get(id);
    }

    public List<Receipt> getReceipts() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" receipt_detail_id");
    }
    public int delete(ReceiptDetail editedReceipt) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.delete(editedReceipt);
    }
}
