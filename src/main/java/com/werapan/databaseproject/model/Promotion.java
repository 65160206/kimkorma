/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wittaya5329
 */
public class Promotion {
    private int id;
    private String name;
    private int percent;
    private int status;
    private Date sdate;
    private int condition;

    public Promotion(int id, String name, int percent, int status, Date sdate, int condition) {
        this.id = id;
        this.name = name;
        this.percent = percent;
        this.status = status;
        this.sdate = sdate;
        this.condition = condition;
    }
    
    public Promotion(String name, int percent, int status, Date sdate, int condition) {
        this.id = -1;
        this.name = name;
        this.percent = percent;
        this.status = status;
        this.sdate = sdate;
        this.condition = condition;
    }
    
    public Promotion(String name, int percent, int status, int condition) {
        this.id = -1;
        this.name = name;
        this.percent = percent;
        this.status = status;
        this.sdate = null;
        this.condition = condition;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.percent = 0;
        this.condition = 0;
        this.status = 0;
        this.sdate = null;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", percent=" + percent + ", status=" + status + ", sdate=" + sdate + ", condition=" + condition + '}';
    }
    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("PMT_ID"));
            promotion.setName(rs.getString("PMT_name"));
            promotion.setCondition(rs.getInt("PMT_condition"));
            promotion.setPercent(rs.getInt("PMT_percent"));
            promotion.setSdate(rs.getTimestamp("PMT_S_date"));
            promotion.setStatus(rs.getInt("PMT_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}
