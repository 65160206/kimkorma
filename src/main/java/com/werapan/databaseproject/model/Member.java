/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthaphan
 */
public class Member {
    private int id;
    private String FirstName;
    private String LastName;
    private String PhoneNumber;
    private int point;
    private Date RegDate;

    public Member(int id, String FirstName, String LastName, String PhoneNumber, int point, Date RegDate) {
        this.id = id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.PhoneNumber = PhoneNumber;
        this.point = point;
        this.RegDate = RegDate;
    }

    public Member(String FirstName, String LastName, String PhoneNumber, int point, Date RegDate) {
        this.id = -1;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.PhoneNumber = PhoneNumber;
        this.point = point;
        this.RegDate = RegDate;
    }
    public Member() {
        this.id = -1;
        this.FirstName = "";
        this.LastName = "";
        this.PhoneNumber = "";
        this.point = 0;
        this.RegDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public Date getRegDate() {
        return RegDate;
    }

    public void setRegDate(Date RegDate) {
        this.RegDate = RegDate;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", FirstName=" + FirstName + ", LastName=" + LastName + ", PhoneNumber=" + PhoneNumber + ", point=" + point + ", RegDate=" + RegDate + '}';
    }
    
    
    
     public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("Mem_ID"));
            member.setFirstName(rs.getString("Mem_F_Name"));
            member.setLastName(rs.getString("Mem_L_Name"));
            member.setPhoneNumber(rs.getString("Mem_Phone"));
            member.setPoint(rs.getInt("Mem_Point"));
            member.setRegDate(rs.getTimestamp("Mem_RegDate"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }
    
}
