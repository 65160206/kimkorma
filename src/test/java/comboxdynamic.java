
import com.werapan.databaseproject.dao.PromotionDao;
import com.werapan.databaseproject.model.Promotion;
import com.werapan.databaseproject.service.PromotionService;
import java.awt.List;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Arthaphan
 */
public class comboxdynamic {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Dynamic Combo Box Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 150);

        
        //dung promotion tumpenname
        PromotionService ps = new PromotionService();
        ArrayList<Promotion> promotions = new ArrayList<>();
         promotions = (ArrayList<Promotion>) ps.getPromotions() ;
    
         ArrayList<String> promotionNames = new ArrayList<>();
         
     for (Promotion promotion : promotions) {
        promotionNames.add(promotion.getName());
    }
     
   
   
        
        JPanel panel = new JPanel();

        DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<>();
        JComboBox<String> comboBox = new JComboBox<>(comboBoxModel);

        
        // Simulating getting objects in a loop
        for (String item : promotionNames) {
           
           comboBoxModel.addElement(item);
        }

        panel.add(comboBox);
        frame.add(panel);

        frame.setVisible(true);
    }
}
